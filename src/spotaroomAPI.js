const apiURL = '/api/public/listings/search/';

const ENDPOINT = {
  propertiesByCity: {
    url: `${apiURL}markers`,
    verb: 'get',
  },
  propertiesDetailedInformation: {
    url: `${apiURL}homecards_ids`,
    verb: 'get',
  },
};

export { ENDPOINT }
